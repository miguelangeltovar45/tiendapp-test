<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MarkController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('mark')->group(function () {
    Route::get('', 'MarkController@index');
    Route::get('{id}', 'MarkController@show');
    Route::post('', 'MarkController@store');
    Route::put('{id}', 'MarkController@update');
    Route::delete('{id}', 'MarkController@destroy');
});

Route::prefix('product')->group(function () {
    Route::get('', 'ProductController@index');
    Route::get('{id}', 'ProductController@show');
    Route::post('', 'ProductController@store');
    Route::put('{id}', 'ProductController@update');
    Route::delete('{id}', 'ProductController@destroy');
});

Route::prefix('product-inventory')->group(function () {
    Route::get('', 'ProductInventoryController@index');
    Route::get('{id}', 'ProductInventoryController@show');
    Route::get('product/{id}', 'ProductInventoryController@showByProduct');
    Route::post('', 'ProductInventoryController@store');
    Route::delete('{id}', 'ProductInventoryController@destroy');
});
