<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductInventory extends Model
{
    //

    protected $attributes = [
        'product_id' => 'number',
        'quantity' => 'number',
        'shipment_date' => 'datetime',
    ];

}
