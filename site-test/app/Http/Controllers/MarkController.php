<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use App\Mark;
use Illuminate\Support\Facades\Validator;
use stdClass;
use Webpatser\Uuid\Uuid;

class MarkController extends Controller
{
    protected $markModel;
    protected $result;
    protected $successCode;
    protected $failedCode;
    protected $noContentCode;
    protected $notFoundCode;
    protected $badRequestCode;
    protected $unauthorizedCode;
    protected $validations;
    protected $validationsMessages;
    protected $uuidGen;

    public function __construct(Mark $mark, stdClass $result)
    {
        $this->markModel = $mark;
        $this->result = $result;
        $this->successCode = Response::HTTP_OK;
        $this->failedCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        $this->noContentCode = Response::HTTP_NO_CONTENT;
        $this->notFoundCode = Response::HTTP_NOT_FOUND;
        $this->badRequestCode = Response::HTTP_BAD_REQUEST;
        $this->unauthorizedCode = Response::HTTP_UNAUTHORIZED;
        $this->validations = [
            'name' => 'required'
        ];
        $this->validationsMessages = [
            'required' => 'El campo :attribute es requerido'
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        //
        try {
            $marks = $this->markModel::all();
            if($marks != null || !empty($marks)){
                $this->result->data = $marks;
                return response()->json($this->result, $this->successCode);
            }
        }catch (\Exception $e){
            Log::error($e);
            switch($e->getCode()){
                case 401:
                    $this->result->error = $e->getMessage();
                    $code = $this->unauthorizedCode;
                    break;
                case 404:
                    $this->result->error = $e->getMessage();
                    $code = $this->notFoundCode;
                    break;
                default:
                    $this->result->error = $e->getMessage().' On '.$e->getLine().' in file '.$e->getFile();
                    $code = $this->failedCode;
                    break;
            }
            return response()->json($this->result, $code);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        //
        try {
            $validator = Validator::make($request->all(), $this->validations, $this->validationsMessages)->validate();
            if($request->all() != null && $validator == true){
                $this->markModel->name = $request->name;
                $this->markModel->reference = Uuid::generate(3,$request->name, Uuid::NS_DNS);
                $this->result->data = $this->markModel->save();
                return response()->json($this->result, $this->successCode);
            }
        }catch (\Exception $e){
            Log::error($e);
            switch($e->getCode()){
                case 401:
                    $this->result->error = $e->getMessage();
                    $code = $this->unauthorizedCode;
                    break;
                case 404:
                    $this->result->error = $e->getMessage();
                    $code = $this->notFoundCode;
                    break;
                default:
                    $this->result->error = $e->getMessage().' On '.$e->getLine().' in file '.$e->getFile();
                    $code = $this->failedCode;
                    break;
            }
            return response()->json($this->result, $code);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        //
        try {
            $mark = $this->markModel->find($id);
            if($mark != null) {
                $this->result->data = $mark;
                return response()->json($this->result, $this->successCode);
            }
        } catch (\Exception $e){
            Log::error($e);
            switch($e->getCode()){
                case 401:
                    $this->result->error = $e->getMessage();
                    $code = $this->unauthorizedCode;
                    break;
                case 404:
                    $this->result->error = $e->getMessage();
                    $code = $this->notFoundCode;
                    break;
                default:
                    $this->result->error = $e->getMessage().' On '.$e->getLine().' in file '.$e->getFile();
                    $code = $this->failedCode;
                    break;
            }
            return response()->json($this->result, $code);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit($id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        //
        try {
            $markModel = $this->markModel;
            $mark = $markModel::find($id);
            $validator = Validator::make($request->all(), $this->validations, $this->validationsMessages)->validate();
            if($mark != null && $validator == true){
                $mark->name = $request->name;
                $this->result->data = $mark->update();
                return response()->json($this->result, $this->successCode);
            }
        }catch (\Exception $e){
            Log::error($e);
            switch($e->getCode()){
                case 401:
                    $this->result->error = $e->getMessage();
                    $code = $this->unauthorizedCode;
                    break;
                case 404:
                    $this->result->error = $e->getMessage();
                    $code = $this->notFoundCode;
                    break;
                default:
                    $this->result->error = $e->getMessage().' On '.$e->getLine().' in file '.$e->getFile();
                    $code = $this->failedCode;
                    break;
            }
            return response()->json($this->result, $code);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $mark = $this->markModel->find($id);
            if($mark != null) {
                $this->result->data = $mark->delete();
                $this->result->message = "Operación realizada";
                return response()->json($this->result, $this->successCode);
            } else {
                $this->result->message = "La marca que intenta eliminar, no existe o ya fué eliminada";
                return response()->json($this->result, $this->notFoundCode);
            }
        } catch (\Exception $e){
            Log::error($e);
            switch($e->getCode()){
                case 401:
                    $this->result->error = $e->getMessage();
                    $code = $this->unauthorizedCode;
                    break;
                case 404:
                    $this->result->error = $e->getMessage();
                    $code = $this->notFoundCode;
                    break;
                default:
                    $this->result->error = $e->getMessage().' On '.$e->getLine().' in file '.$e->getFile();
                    $code = $this->failedCode;
                    break;
            }
            return response()->json($this->result, $code);
        }
    }
}
