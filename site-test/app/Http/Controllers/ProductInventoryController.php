<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use App\ProductInventory;
use stdClass;
use Illuminate\Support\Facades\Validator;

class ProductInventoryController extends Controller
{
    protected $inventoryModel;
    protected $result;
    protected $successCode;
    protected $failedCode;
    protected $noContentCode;
    protected $notFoundCode;
    protected $badRequestCode;
    protected $unauthorizedCode;
    protected $validations;
    protected $validationsMessages;

    public function __construct(ProductInventory $inventoryModel, stdClass $result)
    {
        $this->inventoryModel = $inventoryModel;
        $this->result = $result;
        $this->successCode = Response::HTTP_OK;
        $this->failedCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        $this->noContentCode = Response::HTTP_NO_CONTENT;
        $this->notFoundCode = Response::HTTP_NOT_FOUND;
        $this->badRequestCode = Response::HTTP_BAD_REQUEST;
        $this->unauthorizedCode = Response::HTTP_UNAUTHORIZED;
        $this->validations = [
            'product_id' => 'required|exists:products,id',
            'quantity' => 'required|numeric',
            'shipment_date' => 'required|date_format:Y-m-d H:i:s',
        ];
        $this->validationsMessages = [
            'required' => 'El campo :attribute es requerido',
            'exists' => 'El valor ingresado para :attribute no existe en el sistema',
            'numeric' => 'El valor ingresado para :attribute no es un número',
            'date_format' => 'El valor ingresado para :attribute no tiene el formato para la fecha correcto, debe ser Y-m-d H:i:s',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        //
        try {
            $inventorys = $this->inventoryModel::all();
            if($inventorys != null || !empty($inventorys)){
                $this->result->data = $inventorys;
                return response()->json($this->result, $this->successCode);
            }
        }catch (\Exception $e) {
            Log::error($e);
            switch ($e->getCode()) {
                case 401:
                    $this->result->error = $e->getMessage();
                    $code = $this->unauthorizedCode;
                    break;
                case 404:
                    $this->result->error = $e->getMessage();
                    $code = $this->notFoundCode;
                    break;
                default:
                    $this->result->error = $e->getMessage() . ' On ' . $e->getLine() . ' in file ' . $e->getFile();
                    $code = $this->failedCode;
                    break;
            }
            return response()->json($this->result, $code);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), $this->validations, $this->validationsMessages)->validate();
            if($request->all() != null && $validator == true){
                $this->inventoryModel->product_id = $request->product_id;
                $this->inventoryModel->quantity = $request->quantity;
                $this->inventoryModel->shipment_date = $request->shipment_date;
                $this->result->data = $this->inventoryModel->save();
                return response()->json($this->result, $this->successCode);
            }
        }catch (\Exception $e){
            Log::error($e);
            switch($e->getCode()){
                case 401:
                    $this->result->error = $e->getMessage();
                    $code = $this->unauthorizedCode;
                    break;
                case 404:
                    $this->result->error = $e->getMessage();
                    $code = $this->notFoundCode;
                    break;
                default:
                    $this->result->error = $e->getMessage().' On '.$e->getLine().' in file '.$e->getFile();
                    $code = $this->failedCode;
                    break;
            }
            return response()->json($this->result, $code);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            $inventory = $this->inventoryModel->find($id);
            if($inventory != null) {
                $this->result->data = $inventory;
                return response()->json($this->result, $this->successCode);
            }
        } catch (\Exception $e){
            Log::error($e);
            switch($e->getCode()){
                case 401:
                    $this->result->error = $e->getMessage();
                    $code = $this->unauthorizedCode;
                    break;
                case 404:
                    $this->result->error = $e->getMessage();
                    $code = $this->notFoundCode;
                    break;
                default:
                    $this->result->error = $e->getMessage().' On '.$e->getLine().' in file '.$e->getFile();
                    $code = $this->failedCode;
                    break;
            }
            return response()->json($this->result, $code);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function showByProduct($id)
    {
        try {
            $inventory = $this->inventoryModel->where('product_id', $id)->get();
            if($inventory != null) {
                $this->result->data = $inventory;
                return response()->json($this->result, $this->successCode);
            }
        } catch (\Exception $e){
            Log::error($e);
            switch($e->getCode()){
                case 401:
                    $this->result->error = $e->getMessage();
                    $code = $this->unauthorizedCode;
                    break;
                case 404:
                    $this->result->error = $e->getMessage();
                    $code = $this->notFoundCode;
                    break;
                default:
                    $this->result->error = $e->getMessage().' On '.$e->getLine().' in file '.$e->getFile();
                    $code = $this->failedCode;
                    break;
            }
            return response()->json($this->result, $code);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $inventoryModel = $this->inventoryModel;
            $inventory = $inventoryModel::find($id);
            $validator = Validator::make($request->all(), $this->validations, $this->validationsMessages)->validate();
            if($inventory != null && $validator == true){
                $inventory->product_name = $request->name;
                $inventory->product_size = $request->size;
                $inventory->observations = $request->observations;
                $inventory->mark_id = $request->mark_id;
                $this->result->data = $inventory->update();
                return response()->json($this->result, $this->successCode);
            }
        }catch (\Exception $e){
            Log::error($e);
            switch($e->getCode()){
                case 401:
                    $this->result->error = $e->getMessage();
                    $code = $this->unauthorizedCode;
                    break;
                case 404:
                    $this->result->error = $e->getMessage();
                    $code = $this->notFoundCode;
                    break;
                default:
                    $this->result->error = $e->getMessage().' On '.$e->getLine().' in file '.$e->getFile();
                    $code = $this->failedCode;
                    break;
            }
            return response()->json($this->result, $code);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $inventory = $this->inventoryModel->find($id);
            if($inventory != null) {
                $this->result->data = $inventory->delete();
                $this->result->message = "Operación realizada";
                return response()->json($this->result, $this->successCode);
            } else {
                $this->result->message = "El movimiento de inventario que intenta eliminar, no existe o ya fué eliminada";
                return response()->json($this->result, $this->notFoundCode);
            }
        } catch (\Exception $e){
            Log::error($e);
            switch($e->getCode()){
                case 401:
                    $this->result->error = $e->getMessage();
                    $code = $this->unauthorizedCode;
                    break;
                case 404:
                    $this->result->error = $e->getMessage();
                    $code = $this->notFoundCode;
                    break;
                default:
                    $this->result->error = $e->getMessage().' On '.$e->getLine().' in file '.$e->getFile();
                    $code = $this->failedCode;
                    break;
            }
            return response()->json($this->result, $code);
        }
    }
}
