<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use stdClass;
use App\Product;

class ProductController extends Controller
{
    protected $productModel;
    protected $result;
    protected $successCode;
    protected $failedCode;
    protected $noContentCode;
    protected $notFoundCode;
    protected $badRequestCode;
    protected $unauthorizedCode;
    protected $validations;
    protected $validationsMessages;

    public function __construct(Product $productModel, stdClass $result)
    {
        $this->productModel = $productModel;
        $this->result = $result;
        $this->successCode = Response::HTTP_OK;
        $this->failedCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        $this->noContentCode = Response::HTTP_NO_CONTENT;
        $this->notFoundCode = Response::HTTP_NOT_FOUND;
        $this->badRequestCode = Response::HTTP_BAD_REQUEST;
        $this->unauthorizedCode = Response::HTTP_UNAUTHORIZED;
        $this->validations = [
            'name' => 'required',
            'size' => 'required',
            'observations' => 'required',
            'mark_id' => 'required|exists:marks,id',
        ];
        $this->validationsMessages = [
            'required' => 'El campo :attribute es requerido',
            'exists' => 'El valor ingresado para :attribute no existe en el sistema',
        ];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        //
        try {
                $products = $this->productModel::all();
                if($products != null || !empty($products)){
                    $this->result->data = $products;
                    return response()->json($this->result, $this->successCode);
                }
            }catch (\Exception $e) {
                Log::error($e);
                switch ($e->getCode()) {
                    case 401:
                        $this->result->error = $e->getMessage();
                        $code = $this->unauthorizedCode;
                        break;
                    case 404:
                        $this->result->error = $e->getMessage();
                        $code = $this->notFoundCode;
                        break;
                    default:
                        $this->result->error = $e->getMessage() . ' On ' . $e->getLine() . ' in file ' . $e->getFile();
                        $code = $this->failedCode;
                        break;
                }
                return response()->json($this->result, $code);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        //
        try {
            $validator = Validator::make($request->all(), $this->validations, $this->validationsMessages)->validate();
            if($request->all() != null && $validator == true){
                $this->productModel->product_name = $request->name;
                $this->productModel->product_size = $request->size;
                $this->productModel->observations = $request->observations;
                $this->productModel->mark_id = $request->mark_id;
                $this->result->data = $this->productModel->save();
                return response()->json($this->result, $this->successCode);
            }
        }catch (\Exception $e){
            Log::error($e);
            switch($e->getCode()){
                case 401:
                    $this->result->error = $e->getMessage();
                    $code = $this->unauthorizedCode;
                    break;
                case 404:
                    $this->result->error = $e->getMessage();
                    $code = $this->notFoundCode;
                    break;
                default:
                    $this->result->error = $e->getMessage().' On '.$e->getLine().' in file '.$e->getFile();
                    $code = $this->failedCode;
                    break;
            }
            return response()->json($this->result, $code);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        try {
            $product = $this->productModel->find($id);
            if($product != null) {
                $this->result->data = $product;
                return response()->json($this->result, $this->successCode);
            }
        } catch (\Exception $e){
            Log::error($e);
            switch($e->getCode()){
                case 401:
                    $this->result->error = $e->getMessage();
                    $code = $this->unauthorizedCode;
                    break;
                case 404:
                    $this->result->error = $e->getMessage();
                    $code = $this->notFoundCode;
                    break;
                default:
                    $this->result->error = $e->getMessage().' On '.$e->getLine().' in file '.$e->getFile();
                    $code = $this->failedCode;
                    break;
            }
            return response()->json($this->result, $code);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        try {
            $productModel = $this->productModel;
            $product = $productModel::find($id);
            $validator = Validator::make($request->all(), $this->validations, $this->validationsMessages)->validate();
            if($product != null && $validator == true){
                $product->product_name = $request->name;
                $product->product_size = $request->size;
                $product->observations = $request->observations;
                $product->mark_id = $request->mark_id;
                $this->result->data = $product->update();
                return response()->json($this->result, $this->successCode);
            }
        }catch (\Exception $e){
            Log::error($e);
            switch($e->getCode()){
                case 401:
                    $this->result->error = $e->getMessage();
                    $code = $this->unauthorizedCode;
                    break;
                case 404:
                    $this->result->error = $e->getMessage();
                    $code = $this->notFoundCode;
                    break;
                default:
                    $this->result->error = $e->getMessage().' On '.$e->getLine().' in file '.$e->getFile();
                    $code = $this->failedCode;
                    break;
            }
            return response()->json($this->result, $code);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        try {
            $product = $this->productModel->find($id);
            if($product != null) {
                $this->result->data = $product->delete();
                $this->result->message = "Operación realizada";
                return response()->json($this->result, $this->successCode);
            } else {
                $this->result->message = "El producto que intenta eliminar, no existe o ya fué eliminada";
                return response()->json($this->result, $this->notFoundCode);
            }
        } catch (\Exception $e){
            Log::error($e);
            switch($e->getCode()){
                case 401:
                    $this->result->error = $e->getMessage();
                    $code = $this->unauthorizedCode;
                    break;
                case 404:
                    $this->result->error = $e->getMessage();
                    $code = $this->notFoundCode;
                    break;
                default:
                    $this->result->error = $e->getMessage().' On '.$e->getLine().' in file '.$e->getFile();
                    $code = $this->failedCode;
                    break;
            }
            return response()->json($this->result, $code);
        }
    }
}
