<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    //
    use SoftDeletes;
    protected $attributes = [
        'product_name' => 'string',
        'product_size' => 'string',
        'observations' => 'string',
        'mark_id' => 'number',
    ];
}
