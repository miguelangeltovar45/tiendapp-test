# Prueba laravel Tiendapp
Aquí se condensa toda el api desarrollada para la prueba presentada a Tiendapp

## Dokerización
Se condesó la applicación con docker, usando imágenes para la vesión de PHP, el servidor web NGINX, 
y la bsae de datos MYSQL, para correr todo el proyecto, basta con ejecutar el siguiente comando para construir todo el entorno.
 
```bash
docker-compose up -d --build
```

## Documentación
En el siguiente link de postman, encontrará todos los endpoints disponibles para la prueba
[Documentación del API](https://documenter.getpostman.com/view/5027512/SztHX5dn) 

### Desarrollado por Miguel Angel Tovar
[LinkedIn](https://www.linkedin.com/in/miguel-angel-tovar-velandia-51116996/)